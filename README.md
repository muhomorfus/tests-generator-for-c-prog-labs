# Tests generator for C-prog labs

## Usage

Copy `gen.py` into `func_tests` folder. Then, create files `gen-pos.txt` and `gen-neg.txt` with specified syntax:

```
test1 input -> test1 output <- test1 args;
test2 input -> test2 output <- test2 args;
...
```

Tests can be splitted into test groups with specified description, outputs to `description.md`. Description syntax below:

```
#desc Test group 1 description;
test1 input -> test1 output <- test1 args;
test2 input -> test2 output <- test2 args;
#end;

#desc Tests without args;
test3 input -> test3 output;
test4 input -> test4 output;
test5 input -> test5 output;
#end;
```


`input` and `output` can contain newlines and `\` symbol as newline.

After creating files, execute:

```shell
$ python3 gen.py
```
