def make_filename(name, i, status):
    return "{}_{:02d}_{}.txt".format(name, i, status)


def normalize(string):
    string = string.strip().replace("\\", "\n")
    return "\n".join([s.strip() for s in string.split("\n")])


def get_description(a, b, desc):
    if a == b:
        if desc == "":
            desc = "обычный тест"
        return '- {:02d} - {};\n'.format(a, desc)

    if desc == "":
        desc = "обычные тесты"

    return '- {:02d}-{:02d} - {};\n'.format(a, b, desc)


def read_gen_file(name):
    try:
        gen_filename = "gen-{}.txt".format(name)
        gen_file = open(gen_filename, "r")
        content = gen_file.read()
        tests = [test.strip() for test in content.split(";")]

        description_file = open("description.md", "a")
        description_file.write("{} тесты\n".format(name))
        description = ""
        description_start = 1

        i = 1
        for test in tests:
            print(i, "\"", test, "\"")
            if test.find("->") != -1:
                test_in, test_out = test.split("->")
                test_in = normalize(test_in)

                file_in = open(make_filename(name, i, "in"), "w")
                file_in.write(test_in)
                file_in.close()

                if test_out.find("<-") != -1:
                    test_out, test_args = test_out.split("<-")

                    test_args = normalize(test_args)
                    file_args = open(make_filename(name, i, "args"), "w")
                    file_args.write(test_args)
                    file_args.close()

                test_out = normalize(test_out)

                file_out = open(make_filename(name, i, "out"), "w")
                file_out.write(test_out)
                file_out.close()

                i += 1

            elif test.startswith("#desc"):
                if i != description_start:
                    description_file.write(
                        get_description(description_start, i - 1, description),
                    )
                    description_start = i
                description = test.lstrip("#desc").strip()
            elif test.startswith("#end"):
                description_file.write(
                    get_description(description_start, i - 1, description),
                )
                description_start = i
                description = ""

        if description_start != i:
            description_file.write(
                get_description(description_start, i - 1, description),
            )

        description_file.write("\n")

        description_file.close()
        gen_file.close()
    except FileNotFoundError as e:
        raise e
    except Exception as e:
        gen_file.close()
        raise e


try:
    open("description.md", "w").close()
    read_gen_file("pos")
    read_gen_file("neg")
except Exception as e:
    print("An error occured:", e)
else:
    print("Test was successfully generated")

